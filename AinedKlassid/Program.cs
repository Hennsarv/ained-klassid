﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AinedKlassid
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] klassid = { "1A", "1B", "2A" };
            string[,] kusõpetatakse =
            {
                { "Matemaatika", "1A"},
                { "Matemaatika", "2A"},
                { "Füüsika", "1B"},
                { "Laulmine", "1A"},
                { "Laulmine", "1B"}
            };

            for (int i = 0; i < kusõpetatakse.GetLength(0); i++)
            {
                Console.WriteLine(kusõpetatakse[i, 0] + " " + kusõpetatakse[i, 1]);
                Aine a = Aine.AnnaAine(kusõpetatakse[i, 0]);
                Klass k = Klass.AnnaKlass(kusõpetatakse[i, 1]);
                a.Klassid.Add(k);
                k.Ained.Add(a);

            }
            Console.WriteLine("\nAined:\n");
            foreach (var x in Aine.Ained.Values) Console.WriteLine(x);
            Console.WriteLine("\nKLassid:\n");
            foreach (var x in Klass.Klassid.Values) Console.WriteLine(x);
        }
    }

    class Aine
    {
        public static Dictionary<string, Aine> Ained = new Dictionary<string, Aine>();

        public readonly string Nimetus;
        public List<Klass> Klassid = new List<Klass>();

        public static Aine AnnaAine(string nimetus)
        {
            if (Ained.ContainsKey(nimetus)) return Ained[nimetus];
            else return new Aine(nimetus);
        }

        protected Aine(string nimetus)
        {
            Nimetus = nimetus;
            Ained.Add(nimetus, this);
        }

        public override string ToString()
        {
            return "Õppeainet " + Nimetus + " õpetatakse " 
                + string.Join(",", Klassid.Select(x => x.Nimetus)) 
                + " klassis";
        }

    }

    class Klass
    {
        public static Dictionary<string, Klass> Klassid = new Dictionary<string, Klass>();

        public string Nimetus;
        public List<Aine> Ained = new List<Aine>();

        protected Klass(string nimetus)
        {
            Nimetus = nimetus;
            Klassid.Add(nimetus, this);
        }

        public static Klass AnnaKlass(string nimetus)
        => Klassid.ContainsKey(nimetus) ? Klassid[nimetus]
            : new Klass(nimetus);
        

        public override string ToString()
        {
            return "Klassis " + Nimetus + " õpetatakse: " + string.Join(",", Ained.Select(x => x.Nimetus));
        }
    }
}
